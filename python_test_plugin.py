#!/usr/bin/env python3

#
# maven_plugin.py
#
# Copyright 2018 Alberto Fanjul <albfan@gnome.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import json
import configparser
import collections
import unittest
from pathlib import Path


from gi.repository import Gio
from gi.repository import GLib
from gi.repository import Ide

_ = Ide.gettext

_ATTRIBUTES = ",".join(
    [
        Gio.FILE_ATTRIBUTE_STANDARD_NAME,
        Gio.FILE_ATTRIBUTE_STANDARD_DISPLAY_NAME,
        Gio.FILE_ATTRIBUTE_STANDARD_SYMBOLIC_ICON,
    ]
)

cnf_file = ".gnome-builder-extras"  # will have to change to sth valid.
lsp_cnf_file = ".lsp.json"


def get_workdir_lsp_config(workdir_path):
    workdir_path = Path(workdir_path).resolve()
    cnf_file_from_dir = workdir_path.joinpath(lsp_cnf_file)
    if not cnf_file_from_dir.is_file():
        return None
    with cnf_file_from_dir.open() as of:
        cnf = json.load(of)
    return cnf.get("Python")


def get_workdir_py_config(workdir_path):
    lsp_cnf = get_workdir_lsp_config(workdir_path)
    if lsp_cnf:
        try:
            venv_pth = lsp_cnf["configuration"]["settings"]["pyls"]["plugins"]["jedi"][
                "environment"
            ]
        except KeyError:
            venv_pth = None
        if venv_pth:
            return {"venv_path": Path(venv_pth)}
    workdir_path = Path(workdir_path).resolve()
    cnf_file_from_dir = workdir_path.joinpath(cnf_file)
    if not cnf_file_from_dir.is_file():
        return None
    cnf = configparser.ConfigParser()
    cnf.read(str(cnf_file_from_dir))
    try:
        return cnf["PYTHON"]
    except KeyError:
        return None


def get_possible_venv_path(workdir_path, config=None):
    env_venv = os.environ.get("VIRTUAL_ENV")
    if env_venv:
        return Path(env_venv)
    possible_venv_locs = {
        "venv",
        "~/.pyenv/versions",
        "~/.virtualenvs",
    }
    workdir_path = Path(workdir_path).resolve()
    venv_name = None
    if config is None:
        config = get_workdir_py_config(workdir_path)
    venv_pth = None
    if config:
        venv_pth = config.get("venv_path")
        venv_name = config.get("venv_name")
    # Prioritize Venv Path
    if venv_pth:
        return Path(venv_pth)
    # try venv_name with workdir or venv_name
    venv_name = venv_name or workdir_path.name
    possible_venv_locs.add(venv_name)
    for v_name in possible_venv_locs:
        if v_name.startswith("~"):
            venv_pth = Path(v_name).expanduser().joinpath(venv_name)
        else:
            venv_pth = workdir_path.joinpath(v_name)
        if venv_pth.is_dir():
            venv_pth = venv_pth.joinpath("bin", "python")
        if venv_pth.exists():
            break
    else:  # nobreak
        return
    return venv_pth


class PythonTestProvider(Ide.TestProvider):
    pytest_cmd = ("-m", "pytest")

    def do_parent_set(self, parent):
        if parent is None:
            return
        context = self.get_context()
        build_manager = Ide.BuildManager.from_context(context)
        build_manager.connect("notify::pipeline", self.on_notify_pipeline)
        self.on_notify_pipeline(None, build_manager)

    def on_notify_pipeline(self, pspec, build_manaer):
        self.do_reload()

    def do_run_async(self, test, pipeline, pty, cancellable, callback, data):
        task = Ide.Task.new(self, cancellable, callback)
        task.set_priority(GLib.PRIORITY_LOW)

        context = self.get_context()
        build_system = Ide.BuildSystem.from_context(context)

        task.targets = [build_system]

        try:
            runtime = pipeline.get_runtime()
            runner = runtime.create_runner()
            if not runner:
                task.return_error(Ide.NotSupportedError())

            if pty is not None:
                runner.set_pty(pty)

            srcdir = pipeline.get_srcdir()
            runner.set_cwd(srcdir)

            commands = test.get_command()

            for command in commands:
                runner.append_argv(command)

            test.set_status(Ide.TestStatus.RUNNING)

            def run_test_cb(runner, result, data):
                try:
                    runner.run_finish(result)
                    test.set_status(Ide.TestStatus.SUCCESS)
                except:
                    test.set_status(Ide.TestStatus.FAILED)
                finally:
                    task.return_boolean(True)

            runner.run_async(cancellable, run_test_cb, task)
        except Exception as ex:
            task.return_error(ex)

    def do_run_finish(self, result):
        if result.propagate_boolean():
            return result.targets

    def do_reload(self):

        context = self.get_context()
        build_manager = Ide.BuildManager.from_context(context)
        pipeline = build_manager.get_pipeline()
        if pipeline is None:
            return
        srcdir = pipeline.get_srcdir()
        venv_pth = get_possible_venv_path(srcdir)
        if venv_pth:
            pypth = (
                str(venv_pth.joinpath("bin", "python"))
                if venv_pth.name != "python"
                else str(venv_pth)
            )
            is_pytest = Path(pypth).parent.joinpath(self.pytest_cmd[-1]).exists()
        else:
            pypth = "python"
            pytest_possibilities = [
                Path("/usr/bin/"),
                Path("/usr/local/bin/"),
                Path.home().joinpath(".local/bin/"),
            ]
            for cmd in pytest_possibilities:
                if cmd.joinpath(self.pytest_cmd[-1]).exists():
                    is_pytest = True
                    break
            else:  # nobreak
                is_pytest = False
        if is_pytest:
            launcher = pipeline.create_launcher()
            args = self.pytest_cmd + ("--collect-only", "-pno:sugar", "-q")
            launcher.set_cwd(srcdir)
            launcher.push_argv(pypth)
            for arg in args:
                launcher.push_argv(arg)
            launcher.set_flags(Gio.SubprocessFlags.STDOUT_PIPE | Gio.SubprocessFlags.STDERR_PIPE)
            launcher.set_run_on_host(True)
            subp = launcher.spawn(None)
            subp.communicate_utf8_async(None, None, self.on_pytest_populate, pypth)
        else:
            output = []

            def handle_tests(tests, other=None):
                if tests:
                    output.extend(tests)

            unittest.defaultTestLoader.suiteClass = handle_tests
            try:
                unittest.defaultTestLoader.discover(srcdir)
            except ImportError:
                return
            grouped = collections.defaultdict(set)
            for tests in output:
                if not tests:
                    continue
                test_name, test_info = str(tests).split()
                test_group = test_info[1:-1]
                grouped[test_group].add("{0}.{1}".format(test_group, test_name))
            self.add_grouped_tests(grouped, (pypth, "-m", "unittest", "-v"))

    def on_pytest_populate(self, subp, task, user_data):
        success, stdout, stderr = subp.communicate_utf8_finish(task)
        if not success:
            Ide.warning("Cannot populate python tests: {0}".format(stderr))
            return
        grouped = collections.defaultdict(set)
        for test in stdout.split("\n"):
            test = test.replace("\r", " ").replace("\n", " ").strip()
            if not test or "no tests ran" in test:
                continue
            file_split = test.split("::", maxsplit=1)
            try:
                filename = file_split[0]
            except IndexError:
                filename = test
            grouped[filename].add(test)
        self.add_grouped_tests(grouped, (user_data,) + self.pytest_cmd)

    def add_grouped_tests(self, grouped, test_cmd):
        for filename, tests in grouped.items():
            if filename.endswith(".py"):
                group = filename.replace(".py", "")
            else:
                group = filename
            ptest = PythonTest(group=group, id=filename, display_name=_(filename))
            command = test_cmd + (filename,)
            ptest.set_command(command)
            self.add(ptest)
            if len(tests) <= 1:
                continue
            for test in tests:
                command = test_cmd + (test,)
                ptest = PythonTest(
                    group=group, id=test, display_name=_(test.replace(filename + "::", ""))
                )
                ptest.set_command(command)
                self.add(ptest)


class PythonTest(Ide.Test):
    def get_command(self):
        return self.command

    def set_command(self, command):
        self.command = command
