#!/usr/bin/env python3

# gb_pyls_plugin.py
#
# Copyright 2020 <chalisesagarATgmailDOTcom>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""This plugin provides integration with the Python Language Server from palantir.
-> https://github.com/palantir/python-language-server
"""
import os
import json
import configparser
import doctest
import unicodedata
from pathlib import Path

from gi.repository import GLib
from gi.repository import Gio
from gi.repository import GObject
from gi.repository import Ide

try:
    from gi.repository import Json
except ImportError:
    Ide.debug("GLib Json is required if you need to send config changes.")
    Json = None

DEV_MODE = True if os.getenv("DEV_MODE") else False

cnf_file = ".gnome-builder-extras"  # will have to change to sth valid.
lsp_cnf_file = ".lsp.json"


def get_workdir_lsp_config(workdir_path):
    workdir_path = Path(workdir_path).resolve()
    cnf_file_from_dir = workdir_path.joinpath(lsp_cnf_file)
    if not cnf_file_from_dir.is_file():
        return None
    with cnf_file_from_dir.open() as of:
        cnf = json.load(of)
    return cnf.get("Python")


def get_workdir_py_config(workdir_path):
    lsp_cnf = get_workdir_lsp_config(workdir_path)
    if lsp_cnf:
        try:
            venv_pth = lsp_cnf["configuration"]["settings"]["pyls"]["plugins"]["jedi"][
                "environment"
            ]
        except KeyError:
            venv_pth = None
        if venv_pth:
            return {"venv_path": Path(venv_pth)}
    workdir_path = Path(workdir_path).resolve()
    cnf_file_from_dir = workdir_path.joinpath(cnf_file)
    if not cnf_file_from_dir.is_file():
        return None
    cnf = configparser.ConfigParser()
    cnf.read(str(cnf_file_from_dir))
    try:
        return cnf["PYTHON"]
    except KeyError:
        return None


def get_possible_venv_path(workdir_path, config=None):
    env_venv = os.environ.get("VIRTUAL_ENV")
    if env_venv:
        return Path(env_venv)
    possible_venv_locs = {
        "venv",
        "~/.pyenv/versions",
        "~/.virtualenvs",
    }
    workdir_path = Path(workdir_path).resolve()
    venv_name = None
    if config is None:
        config = get_workdir_py_config(workdir_path)
    venv_pth = None
    if config:
        venv_pth = config.get("venv_path")
        venv_name = config.get("venv_name")
    # Prioritize Venv Path
    if venv_pth:
        return Path(venv_pth)
    # try venv_name with workdir or venv_name
    venv_name = venv_name or workdir_path.name
    possible_venv_locs.add(venv_name)
    for v_name in possible_venv_locs:
        if v_name.startswith("~"):
            venv_pth = Path(v_name).expanduser().joinpath(venv_name)
        else:
            venv_pth = workdir_path.joinpath(v_name)
        if venv_pth.is_dir():
            venv_pth = venv_pth.joinpath("bin", "python")
        if venv_pth.exists():
            break
    else:  # nobreak
        return
    return venv_pth


class PylsService(Ide.Object):
    """Language Server Service."""

    _client = None
    _has_started = False
    _supervisor = None
    pysettings = None

    @classmethod
    def from_context(klass, context):
        return context.ensure_child_typed(PylsService)

    @GObject.Property(type=Ide.LspClient)
    def client(self):
        return self._client

    @client.setter
    def client(self, value):
        self._client = value
        self.notify("client")

    def do_stop(self):
        """Stops the Python Language Server upon request to shutdown the
        PylsService.
        """
        if self._client is not None:
            Ide.warning("Shutting down server")
            self._client.stop()
            self._client.destroy()

        if self._supervisor is not None:
            supervisor, self._supervisor = self._supervisor, None
            supervisor.stop()

    @staticmethod
    def prepare_config(cnf):
        if not Json:
            return None
        config = cnf.get("configuration")
        if not config:
            return None
        return Json.gvariant_deserialize_data(json.dumps(config), -1, None)

    def _ensure_started(self):
        """Start the language server service."""
        # To avoid starting the `pyls` process unconditionally at startup,
        # we lazily start it when the first provider tries to bind a client
        # to its :client property.
        if not self._has_started:
            self._has_started = True
            workdir = self.get_context().ref_workdir()
            workdir_path = workdir.get_path()
            cnf = get_workdir_lsp_config(workdir_path)
            venv_pth = get_possible_venv_path(workdir_path)
            ls_args = (cnf or {}).get("cmd")
            if cnf is None or ls_args is None:
                if not venv_pth:
                    Ide.warning(
                        "No '.lsp.json' or '.gnome-builder-extras' file or 'Python' key in: {0}. Using default.".format(
                            workdir_path
                        )
                    )
            venv_pth = str(venv_pth) if venv_pth else "python"
            # Setup a launcher to spawn the python language server
            launcher = self._create_launcher()
            launcher.set_clear_env(False)
            # Locate the directory of the project and run ls from there.
            launcher.set_cwd(workdir_path)
            if cnf:
                envs = cnf.get("env") or {}
                self.pysettings = self.prepare_config(cnf)
                for env_name, env_value in envs.items():
                    launcher.setenv(env_name, env_value, True)
            if ls_args:
                ls_args = ls_args.split()
                for arg in ls_args:
                    launcher.push_argv(arg)
            else:
                launcher.push_argv(venv_pth)
                launcher.push_argv("-m")
                launcher.push_argv("pyls")
                launcher.push_argv("--check-parent-process")
            if os.path.exists(
                os.path.join(workdir_path, "src")
            ):  # just for src/ related projects.
                launcher.setenv("PYTHONPATH", os.path.join(workdir_path, "src"), True)
            # Spawn our peer process and monitor it for
            # crashes. We may need to restart it occasionally.
            self._supervisor = Ide.SubprocessSupervisor()
            self._supervisor.connect("spawned", self._pyls_spawned)
            self._supervisor.set_launcher(launcher)
            self._supervisor.start()

    def _did_change_configuration(self, source_object, result, user_data):
        try:
            self._client.send_notification_finish(result)
        except BaseException as exc:
            Ide.debug("Change Configuration Notification error: {}".format(exc.args))

    def _pyls_spawned(self, supervisor, subprocess):
        stdin = subprocess.get_stdin_pipe()
        stdout = subprocess.get_stdout_pipe()
        io_stream = Gio.SimpleIOStream.new(stdout, stdin)

        if self._client:
            self._client.stop()
            self._client.destroy()

        self._client = Ide.LspClient.new(io_stream)
        try:
            self._client.connect("initialized", self._on_initialized)
        except TypeError:
            call_load = True
        else:
            call_load = False
        self.append(self._client)
        self._client.start()
        self.notify("client")
        if call_load:
            self._on_initialized(self._client)

    def _on_initialized(self, client):
        if not self.pysettings:
            return
        cancellable = client.ref_cancellable()
        client.send_notification_async(
            "workspace/didChangeConfiguration",
            self.pysettings,
            cancellable,
            self._did_change_configuration,
            None,
        )

    def _create_launcher(self):
        """Creates a launcher to be used by the python service."""
        flags = Gio.SubprocessFlags.STDIN_PIPE | Gio.SubprocessFlags.STDOUT_PIPE
        if not DEV_MODE:
            flags |= Gio.SubprocessFlags.STDERR_SILENCE
        launcher = Ide.SubprocessLauncher()
        launcher.set_flags(flags)
        launcher.set_cwd(GLib.get_home_dir())
        launcher.set_run_on_host(True)
        return launcher

    @classmethod
    def bind_client(cls, provider):
        context = provider.get_context()
        self = cls.from_context(context)
        self._ensure_started()
        self.bind_property("client", provider, "client", GObject.BindingFlags.SYNC_CREATE)


class PylsDiagnosticProvider(Ide.LspDiagnosticProvider, Ide.DiagnosticProvider):
    """Disable this if you need diagnostics via gca builtin."""

    def do_load(self):
        PylsService.bind_client(self)


class PylsCompletionProvider(Ide.LspCompletionProvider, Ide.CompletionProvider):
    """Disable this if you need only one completion."""

    def do_load(self, context):
        PylsService.bind_client(self)

    def do_get_priority(self, context):
        return -1000

    def do_get_comment(self, proposal):
        comment = proposal.get_detail()
        if comment:
            comment = unicodedata.normalize("NFKD", comment)
            if comment and ">>>" in comment:
                try:
                    dt_parse = doctest.DocTestParser().parse(comment)
                except ValueError:
                    dt_parse = []
                comments = []
                for doc in dt_parse:
                    if not isinstance(doc, str):
                        continue
                    sr_doc = doc.strip()
                    if not sr_doc or sr_doc == "\n":
                        continue
                    comments.append(sr_doc)

                comment = "\n".join(comments)
                proposal.get_detail = lambda: comment

        return comment


class PylsRenameProvider(Ide.LspRenameProvider, Ide.RenameProvider):
    def do_load(self):
        PylsService.bind_client(self)


class PylsHighlighter(Ide.LspHighlighter, Ide.Highlighter):
    def do_load(self):
        PylsService.bind_client(self)


class PylsFormatter(Ide.LspFormatter, Ide.Formatter):
    def do_load(self):
        PylsService.bind_client(self)


class PylsSymbolResolver(Ide.LspSymbolResolver, Ide.SymbolResolver):
    def do_load(self):
        PylsService.bind_client(self)


class PylsHoverProvider(Ide.LspHoverProvider, Ide.HoverProvider):
    def do_prepare(self):
        self.props.category = "Python Doc"
        self.props.priority = 200
        PylsService.bind_client(self)
