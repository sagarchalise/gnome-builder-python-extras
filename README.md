
## Some patching in plugins for [Gnome Builder](https://wiki.gnome.org/Apps/Builder) for python development.

### Expects `.gnome-builder-extras` config file in `workdir` which can have:


```
[PYTHON]
venv_name=venv_name_to_search
{venv_path=venv_path} ;if not venv_name
```

For Language Server going with `.lsp.json` as it can be used by any language servers.

```json
{"Python": {
   "cmd": "pyls -v",
   "configuration": {
        "settings": {AS_SUPPORTED_BY_PYLS}
   }
}}
```
If not `.lsp.json` present, `venv_name` will take priority and search for language servers else defaults to system.

The configs are either or basis.

#### Comes with

* `jedi_plugin.py`
    - Some modification to `jedi_plugin.py` inorder to incorporate `virtualenvs` and some changes in showing completion.
    - :warning: **Seems `jedi` will be deprecated for LSP in builder.**

* Language Server Plugin
    - Mostly based on other language server plugin in `gnome-builder`
    - Depends on `pyls`. `pip install python-language-server`
        + Would like to incorporate other LS but it is not working currently with `pylance`.
    - Copy `python_langserv_plugin.py` and `python-langserv.plugin` into `gnome-builder` plugin path.
        + You may require to add `X-Builder-ABI=CURRENT_API` where CURRENT_API can be taken from any another plugin.
    - This can be used for other language servers as well. Just emulate. P.S. I use something similar for `java`.

* Python Test Plugin
    - Lists unitests. The configs are picked to match virtualenvs if present in `.lsp.json` or `.gnome-builder-extras`.

* Python Build Plugin
    - Based on `pyproject.toml` or `setup.py` and can use `poetry` as well.

* Python Project Setup
    - All things are optional but:
        + Uses `cookiecutter`.
        + Automatic `venv`
        + Automatic `git`


See [Tips & Tricks](tips-and-tricks.md) for some additional python helpers that builder incorporates.




