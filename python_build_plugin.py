#!/usr/bin/env python3

#
# maven_plugin.py
#
# Copyright 2018 Alberto Fanjul <albfan@gnome.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import os
import json
import configparser
from pathlib import Path

from gi.repository import Gio
from gi.repository import GLib
from gi.repository import GObject
from gi.repository import Ide

_ = Ide.gettext

_ATTRIBUTES = ",".join(
    [
        Gio.FILE_ATTRIBUTE_STANDARD_NAME,
        Gio.FILE_ATTRIBUTE_STANDARD_DISPLAY_NAME,
        Gio.FILE_ATTRIBUTE_STANDARD_SYMBOLIC_ICON,
    ]
)

cnf_file = ".gnome-builder-extras"  # will have to change to sth valid.
lsp_cnf_file = ".lsp.json"
pyname = "python"


def get_workdir_lsp_config(workdir_path):
    workdir_path = Path(workdir_path).resolve()
    cnf_file_from_dir = workdir_path.joinpath(lsp_cnf_file)
    if not cnf_file_from_dir.is_file():
        return None
    with cnf_file_from_dir.open() as of:
        cnf = json.load(of)
    return cnf.get(pyname.title()) or cnf.get(pyname)


def get_workdir_py_config(workdir_path):
    lsp_cnf = get_workdir_lsp_config(workdir_path)
    if lsp_cnf:
        try:
            venv_pth = lsp_cnf["configuration"]["settings"]["pyls"]["plugins"]["jedi"][
                "environment"
            ]
        except KeyError:
            venv_pth = None
        if venv_pth:
            return {"venv_path": Path(venv_pth)}
    workdir_path = Path(workdir_path).resolve()
    cnf_file_from_dir = workdir_path.joinpath(cnf_file)
    if not cnf_file_from_dir.is_file():
        return None
    cnf = configparser.ConfigParser()
    cnf.read(str(cnf_file_from_dir))
    try:
        return cnf[pyname.upper()] or cnf[pyname]
    except KeyError:
        return None


def get_possible_venv_path(workdir_path, config=None):
    env_venv = os.environ.get("VIRTUAL_ENV")
    if env_venv:
        return Path(env_venv)
    possible_venv_locs = {
        "venv",
        "~/.pyenv/versions",
        "~/.virtualenvs",
    }
    workdir_path = Path(workdir_path).resolve()
    venv_name = None
    if config is None:
        config = get_workdir_py_config(workdir_path)
    venv_pth = None
    if config:
        venv_pth = config.get("venv_path")
        venv_name = config.get("venv_name")
    # Prioritize Venv Path
    if venv_pth:
        return Path(venv_pth)
    # try venv_name with workdir or venv_name
    venv_name = venv_name or workdir_path.name
    possible_venv_locs.add(venv_name)
    for v_name in possible_venv_locs:
        if v_name.startswith("~"):
            venv_pth = Path(v_name).expanduser().joinpath(venv_name)
        else:
            venv_pth = workdir_path.joinpath(v_name)
        if venv_pth.is_dir():
            venv_pth = venv_pth.joinpath("bin", "python")
        if venv_pth.exists():
            break
    else:  # nobreak
        return
    return venv_pth


class PythonBuildSystemDiscovery(Ide.SimpleBuildSystemDiscovery):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.props.glob = "pyproject.toml"
        self.props.hint = "python_build_plugin"
        self.props.priority = 2000


class PythonBuildSystemSetupDiscovery(PythonBuildSystemDiscovery):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.props.glob = "setup.cfg"


class PythonBuildSystemSetupPythonDiscovery(PythonBuildSystemSetupDiscovery):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.props.glob = "setup.py"


class PythonBuildSystem(Ide.Object, Ide.BuildSystem):
    project_file = GObject.Property(type=Gio.File)

    def do_get_id(self):
        return pyname

    def do_get_display_name(self):
        return _(pyname.title())

    def do_get_priority(self):
        return 2000


class PythonPipelineAddin(Ide.Object, Ide.PipelineAddin):
    """
    The MavenPipelineAddin is responsible for creating the necessary build
    stages and attaching them to phases of the build pipeline.
    """

    def do_load(self, pipeline):
        context = self.get_context()
        build_system = Ide.BuildSystem.from_context(context)

        if not isinstance(build_system, PythonBuildSystem):
            return

        config = pipeline.get_config()
        builddir = pipeline.get_builddir()
        runtime = config.get_runtime()
        srcdir = pipeline.get_srcdir()
        venv_pth = get_possible_venv_path(srcdir)
        poetry_name = "poetry"
        if not venv_pth and not runtime.contains_program_in_path(pyname):
            raise OSError("The runtime must contain python to build python projects")
        elif venv_pth:
            poetry_cmd = venv_pth.parent.joinpath(poetry_name)
            is_poetry = poetry_cmd.exists()
        else:
            venv_pth = "python"
            poetry_possiblities = [
                Path("/usr/bin/"),
                Path("/usr/local/bin/"),
                Path.home().joinpath(".local/bin/"),
            ]
            for cmd in poetry_possiblities:
                poetry_cmd = cmd.joinpath(poetry_name)
                if poetry_cmd.exists():
                    is_poetry = True
                    break
            else:  # nobreak
                poetry_cmd = None
                is_poetry = False
        is_poetry = is_poetry and os.path.exists(os.path.join(srcdir, "poetry.lock"))
        poetry_cmd = str(poetry_cmd)
        venv_pth = str(venv_pth)
        if is_poetry:
            build_cmd = [poetry_cmd, "build"]
            clean_cmd = [poetry_cmd, "check"]
            install_cmd = [venv_pth, "-m", "pip", "install", "."]
        else:
            build_cmd = [venv_pth, "setup.py", "build", "--build-base", builddir]
            clean_cmd = [venv_pth, "setup.py", "clean"]
            install_cmd = [venv_pth, "setup.py", "install"]
        build_launcher = pipeline.create_launcher()
        build_launcher.set_cwd(srcdir)
        for bd in build_cmd:
            build_launcher.push_argv(bd)

        clean_launcher = pipeline.create_launcher()
        clean_launcher.set_cwd(srcdir)
        for cd in clean_cmd:
            clean_launcher.push_argv(cd)

        build_stage = Ide.PipelineStageLauncher.new(context, build_launcher)
        build_stage.set_name(_("Building project"))
        build_stage.set_clean_launcher(clean_launcher)
        build_stage.connect("query", self._query)
        self.track(pipeline.attach(Ide.PipelinePhase.BUILD, 0, build_stage))

        install_launcher = pipeline.create_launcher()
        install_launcher.set_cwd(srcdir)
        for ins in install_cmd:
            install_launcher.push_argv(ins)
        install_stage = Ide.PipelineStageLauncher.new(context, install_launcher)
        install_stage.set_name(_("Installing project"))
        self.track(pipeline.attach(Ide.PipelinePhase.INSTALL, 0, install_stage))
        if is_poetry:
            export_launcher = pipeline.create_launcher()
            export_launcher.set_cwd(srcdir)
            export_launcher.push_argv(poetry_cmd)
            export_launcher.push_argv("publish")
            export_stage = Ide.PipelineStageLauncher.new(context, export_launcher)
            export_stage.set_name(_("Publishing Project"))
            self.track(pipeline.attach(Ide.PipelinePhase.EXPORT, 0, export_stage))

    def _query(self, stage, pipeline, targets, cancellable):
        stage.set_completed(False)


class PythonBuildTarget(Ide.Object, Ide.BuildTarget):
    def do_get_install_directory(self):
        return None

    def do_get_name(self):
        return _("python-setup")

    def do_get_language(self):
        return "python3"

    def do_get_cwd(self):
        context = self.get_context()
        project_file = Ide.BuildSystem.from_context(context).project_file
        if project_file.query_file_type(0, None) == Gio.FileType.DIRECTORY:
            return project_file.get_path()
        else:
            return project_file.get_parent().get_path()

    def do_get_argv(self):
        """
        This requires an argument -Dexec.mainClass="my.package.MainClass"
        Use run-opts
        """
        cwd = self.do_get_cwd()
        venv_pth = get_possible_venv_path(cwd)
        return [str(venv_pth) if venv_pth else "python"]

    def do_get_priority(self):
        return 0


class PythonBuildTargetProvider(Ide.Object, Ide.BuildTargetProvider):
    def do_get_targets_async(self, cancellable, callback, data):
        task = Ide.Task.new(self, cancellable, callback)
        task.set_priority(GLib.PRIORITY_LOW)

        context = self.get_context()
        build_system = Ide.BuildSystem.from_context(context)

        if not isinstance(build_system, PythonBuildSystem):
            task.return_error(
                GLib.Error(
                    "Not python build system",
                    domain=GLib.quark_to_string(Gio.io_error_quark()),
                    code=Gio.IOErrorEnum.NOT_SUPPORTED,
                )
            )
            return

        task.targets = [build_system.ensure_child_typed(PythonBuildTarget)]
        task.return_boolean(True)

    def do_get_targets_finish(self, result):
        if result.propagate_boolean():
            return result.targets
