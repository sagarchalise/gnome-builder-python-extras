#! /usr/bin/python3
"""Formatter for calling from commandline for gnome-builder."""
import io
import sys

import black


def format_code(content, line_length):
    """Code format and write to stdout.

    Args:
        content (bytes): source content
        line_length (int): line length to be used
    """
    try:
        str_content, enc, newline = black.decode_bytes(content)
        mode = black.FileMode(line_length=line_length)
        changed_content = black.format_file_contents(str_content, mode=mode, fast=True)
    except black.NothingChanged:
        sys.exit(0)
    else:
        output = io.TextIOWrapper(
            sys.stdout.buffer, encoding=enc, newline=newline, write_through=True
        )
        output.write(changed_content)
        output.detach()


def get_sys_arg(index):
    """Get sys.arv based on index.

    Args:
        index (int): Index in sys.argv


    Returns:
        object: sys.argv indexed value or None
    """
    try:
        return sys.argv[index]
    except IndexError:
        return None


if __name__ == "__main__":
    content_file = get_sys_arg(2)
    try:
        content = None
        with open(content_file, "rb") as f:
            content = f.read()
    except FileNotFoundError:
        sys.exit(0)
    if not content:
        sys.exit(0)
    line_length = get_sys_arg(1)
    if line_length:
        line_length = int(line_length)
    else:
        line_length = 99
    format_code(content, line_length)
