#!/usr/bin/env python3

#
# jedi_plugin.py
#
# Copyright 2015 Elad Alfassa <elad@fedoraproject.org>
# Copyright 2015-2019 Christian Hergert <chris@dronelabs.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import subprocess
import configparser

from pathlib import Path


from gi.repository import Ide
from gi.repository import GLib
from gi.repository import Gio
from gi.repository import GObject


cnf_file = ".gnome-builder-extras"  # will have to change to sth valid.


def get_workdir_doc_format(workdir_path):
    workdir_path = Path(workdir_path).resolve()
    cnf_file_from_dir = workdir_path.joinpath(cnf_file)
    if not cnf_file_from_dir.is_file():
        return "google"
    cnf = configparser.ConfigParser()
    cnf.read(str(cnf_file_from_dir))
    try:
        return cnf["PYTHON"]["doc_format"]
    except KeyError:
        return "google"


_PROXY_TASK = None


def get_proxy_worker_cb(app, result, task):
    try:
        worker = app.get_worker_finish(result)
        task.return_object(worker)
    except Exception as ex:
        return_error(task, ex)


def return_error(task, error):
    # print(repr(error))
    task.return_error(GLib.Error(message=repr(error)))


def get_proxy_finish(result):
    return result.propagate_object()


def pydoc_gen_cb(proxy, result, task):
    try:
        variant = proxy.call_finish(result)
        task.variant = variant
        task.return_boolean(True)
    except Exception as ex:
        return_error(task, ex)


def pydoc_gen_async(proxy, filename, doc_format, indent_width, cancellable, callback, data=None):
    task = Ide.Task.new(proxy, cancellable, callback, data)
    task.set_name("python-docgen-complete")

    proxy.call(
        "GenerateDoc",
        GLib.Variant("(sss)", (filename, doc_format, str(indent_width))),
        0,
        10000,
        cancellable,
        pydoc_gen_cb,
        task,
    )


def on_activate_menu(action, variant, view):
    def on_gen_doc_complete(obj, task, data):
        task.propagate_boolean()
        variants = task.variant
        text = variants.get_child_value(0).get_string()
        if text:
            buffer = data.get_buffer()
            buffer.set_text(text, -1)

    def get_proxy_cb(obj, result):
        try:
            buffer = obj.get_buffer()
            width = obj.get_indent_width()
            if width == -1:
                width = obj.get_tab_width()
            doc_format = get_workdir_doc_format(buffer.ref_context().ref_workdir().get_path())
            proxy = get_proxy_finish(result)
            begin, end = buffer.get_bounds()
            pydoc_gen_async(
                proxy,
                buffer.get_file().get_path(),
                doc_format,
                width,
                None,
                on_gen_doc_complete,
                view,
            )
        except Exception as ex:
            return_error(task, ex)

    task = Ide.Task.new(view, None, get_proxy_cb)
    task.set_name("python-docgen-worker-request")
    app = Gio.Application.get_default()
    app.get_worker_async("python_docgen_plugin", None, get_proxy_worker_cb, task)


class PythonEditorPageAddin(GObject.Object, Ide.EditorPageAddin):
    def on_popup(self, view, menu):
        self.menu.remove_all()
        self.menu.append_item(self.menu_item)

    def do_load(self, page):
        self.menu = Gio.Menu.new()
        source_view = page.get_view()
        file_menu = Ide.Application.get_menu_by_id(
            Ide.Application.get_default(), "ide-source-view-popup-menu-files-section"
        )
        action_group = source_view.get_action_group("view")
        action_group.add_action_entries(
            [["generate-py-docstring", on_activate_menu, None]], source_view
        )
        file_menu.append_section(None, self.menu)
        self.menu_item = Gio.MenuItem.new(label=_("Python: Generate Docstring"))
        self.menu_item.set_action_and_target_value("view.generate-py-docstring")
        source_view.connect("populate-popup", self.on_popup)


class DocGenerationRequest:
    did_run = False
    cancelled = False
    cmd = ["python", "-m", "doq.cli"]

    def __init__(self, invocation, filename, formatter, indent_width):
        self.invocation = invocation
        self.filename = filename
        self.formatter = formatter
        self.indent_width = indent_width

    def run(self):
        try:
            if not self.cancelled:
                self._run()
        except Exception as ex:
            self.invocation.return_error_literal(
                Gio.dbus_error_quark(), Gio.DBusError.IO_ERROR, repr(ex)
            )

    def _run(self):
        self.did_run = True
        cmd = self.cmd[:]
        cmd.append("--file=" + self.filename)
        cmd.append("--formatter=" + self.formatter)
        cmd.append("--indent=" + self.indent_width)
        data = ""
        with subprocess.Popen(cmd, stdout=subprocess.PIPE, text=True) as proc:
            data = proc.stdout.read()
        self.invocation.return_value(GLib.Variant("(s)", (data,)))

    def cancel(self):
        if not self.cancelled and not self.did_run:
            self.cancelled = True
            self.invocation.return_error_literal(
                Gio.io_error_quark(), Gio.IOErrorEnum.CANCELLED, "Operation was cancelled"
            )


dbus_name = "org.gnome.builder.plugins.python_docgen_plugin"


class DocGenService(Ide.DBusService):
    queue = None
    handler_id = None

    def __init__(self):
        super().__init__()
        self.queue = {}
        self.handler_id = 0

    @Ide.DBusMethod(
        dbus_name,
        in_signature="sss",
        out_signature="s",
        is_async=True,
    )
    def GenerateDoc(self, invocation, filename, formatter, indent_info):
        if filename in self.queue:
            request = self.queue.pop(filename)
            request.cancel()
        self.queue[filename] = DocGenerationRequest(invocation, filename, formatter, indent_info)
        if not self.handler_id:
            self.handler_id = GLib.timeout_add(5, self.process)

    def process(self):
        self.handler_id = 0
        while self.queue:
            filename, request = self.queue.popitem()
            request.run()
        return False


class DocGenWorker(GObject.Object, Ide.Worker):
    _service = None

    def do_register_service(self, connection):
        self._service = DocGenService()
        self._service.export(connection, "/")

    def do_create_proxy(self, connection):
        return Gio.DBusProxy.new_sync(
            connection,
            (
                Gio.DBusProxyFlags.DO_NOT_LOAD_PROPERTIES
                | Gio.DBusProxyFlags.DO_NOT_CONNECT_SIGNALS
                | Gio.DBusProxyFlags.DO_NOT_AUTO_START_AT_CONSTRUCTION
            ),
            None,
            None,
            "/",
            dbus_name,
            None,
        )
