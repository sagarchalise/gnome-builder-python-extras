## Formatting Python Code
* We can use `beautifier` plugin for that. If you use `black` like me. You can set 

```
[global]
default = black

[black]
command-pattern = black_format_output.py 99 @s@
name = Black
```


And use [`black_format_output.py`](black_format_output.py) from paths such as `/usr/local/bin`. This will write to `stdout` which beautifier uses for selection and you will receive menu in editor as well.
* Not sure why `language-server` for this is not working.

### Diagnostics
* Although `gnome-builder` says `pylint` in preference, it uses [gnome-code-assitance](https://gnome.gitlab.org/GNOME/gnome-code-assistance) for diagnostics. You can build it then use.
    * If you use `flake8`, replace [`gca.py`](gca.py) into `backends/python/__init__.py`.
* This is enabled in `langserv.plugin` as well if you donot want to go this route.
    


